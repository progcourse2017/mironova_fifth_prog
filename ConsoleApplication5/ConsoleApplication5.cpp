// ConsoleApplication5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <windows.h>

inline bool IsKeyDown(int Key)
{
	return (GetKeyState(Key) & 0x8000) != 0;
}

using namespace std;

int main()
{
	while (true)
	{
		if (IsKeyDown(VK_ESCAPE))
		{
			break;
		}
		else {

			string password;
			string generatedPass;

			cout << "Enter a password, containing no more than 4 characters:" << endl;
			cin >> password;
			if (password.length() > 4)
			{
				cout << "Error. Enter a password, containing no more than 4 characters:" << endl;

			}
			else
			{
				char digitArray[] = { 'a' - 1,'a','a','a' };

				while (password.compare(generatedPass) != 0) {

					digitArray[0]++;
					for (int x = 0; x < password.length(); x++) {
						if (digitArray[x] == 'z' + 1)
						{
							digitArray[x] = 'a';
							digitArray[x + 1]++;
						}
					}

					generatedPass = digitArray[password.length() - 1];
					for (int i = password.length() - 2; i >= 0; i--)
						generatedPass += digitArray[i];

					cout << generatedPass << endl;
				}

				cout << "Your password:" << generatedPass << endl;
			}
		}
	}
		system("pause");

		return 0;
	}

